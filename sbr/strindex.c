/*
** strindex.c -- "unsigned" lexical index
**            -- Returns the index at which `needle' appears in `haystack' 
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>

int
stringdex(char *needle, char *haystack)
{
	char *p;

	if (needle == NULL || haystack == NULL)
		return -1;

	for (p = haystack; *p; p++)
		if (strncasecmp(p, needle, strlen(needle))==0)
			return (p - haystack);

	return -1;
}
