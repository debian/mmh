/*
** dtimep.lex exceeds the default table capacities for some old versions
** of lex (and the minimum defaults as specified by POSIX).  The following
** choices meet or exceed the lex defaults for older SunOS4.x, Solaris,
** HPUX, and AIX.
*/
%e4000
%p7000
%n2500
%a5000
%{
#include <time.h>
#include <ctype.h>
#include <h/tws.h>

/*
** Since we're looking at a string at a time, don't worry about
** wrapping to the next buffer.
*/
#define yywrap() 1
#define YY_SKIP_YYWRAP

#define YY_NO_INPUT

/*
** This is the tricky thing that makes this function cool.  We
** replace the traditional int yylex(void) declaration with our
** dparsetime() declaration, essentially piggy-backing off the
** utility of the yylex() function and adding what we need to make
** the parsing function useful to us.
*/
#define YY_DECL struct tws *dparsetime(char *lexstr)

/*
** yyerminate() is called after the input string is matched to
** completion (actually, when the lexer reaches an EOF).  The only
** thing that really needs to be in this macro function is the
** return call, which must be substituted inline into dparsetime.
*/

#define yyterminate() (void)yy_delete_buffer(lexhandle); \
	if(!(tw.tw_flags & TW_SUCC)) { \
		return (struct tws *)NULL; \
	} \
	if(tw.tw_year < 1970) \
		tw.tw_year += 1900; \
	if(tw.tw_year < 1970) \
		tw.tw_year += 100; \
	return(&tw)

/*
** Patchable flag that says how to interpret NN/NN/NN dates. When
** true, we do it European style: DD/MM/YY. When false, we do it
** American style: MM/DD/YY.  Of course, these are all non-RFC822
** compliant.
*/
int europeandate = 0;

static int
name2num(char *name, char *names[])
{
	int i;

	for (i=0; names[i]; i++) {
		if (strncasecmp(name, names[i], strlen(names[i]))==0) {
			return i;
		}
	}
	return 0;
}

/*
** The SET* macros will parse for the appropriate field, and leave the
** cp pointer at the first character after the desired field. Be
** careful with variable-length fields or alpha-num mixes.
**
** The SKIP* macros skip over characters of a particular class and
** leave cp at the position of the first character that doesn't match
** that class. Correspondingly, SKIPTO* skips until it reaches a
** character of a particular class.
*/

#define INIT() { cp = yytext;}
#define SETWDAY()  { tw.tw_wday = name2num(cp, tw_dotw); \
	tw.tw_flags &= ~TW_SDAY; tw.tw_flags |= TW_SEXP; SKIPA(); }
#define SETMON()  { tw.tw_mon = name2num(cp, tw_moty); SKIPA(); }
#define SETMON_NUM()  { tw.tw_mon = atoi(cp)-1; SKIPD(); }
#define SETYEAR()  { tw.tw_year = atoi(cp); SKIPD(); }
#define SETDAY()  { tw.tw_mday = atoi(cp); tw.tw_flags |= TW_YES; SKIPD(); }
#define SETTIME()  { tw.tw_hour = atoi(cp); cp += 2; SKIPTOD(); \
	tw.tw_min = atoi(cp); cp += 2; if(*cp == ':') { \
	tw.tw_sec = atoi(++cp); SKIPD(); } }
#define SETZONE(x)  { tw.tw_zone = ((x)/100)*60+(x)%100; \
	tw.tw_flags |= TW_SZEXP; SKIPD(); }
#define SETZONEC(h, m)  { tw.tw_zone = (h)*60+(m); \
	tw.tw_flags |= TW_SZEXP; SKIPD(); }
#define SETDST()  { tw.tw_flags |= TW_DST; }
#define SKIPD()  { while ( isdigit(*cp++) ) ; --cp; }
#define SKIPTOD()  { while ( !isdigit(*cp++) ) ; --cp; }
#define SKIPA()  { while ( isalpha(*cp++) ) ; --cp; }
#define SKIPTOA()  { while ( !isalpha(*cp++) ) ; --cp; }
#define SKIPSP()  { while ( isspace(*cp++) ) ; --cp; }
#define SKIPTOSP()  { while ( !isspace(*cp++) ) ; --cp; }

#ifdef ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST
# ifdef HAVE_SYS_TIME_H
#  include <sys/time.h>
# endif
#include <time.h>

static void
zonehack (struct tws *tw)
{
	struct tm *tm;

	if (dmktime (tw) == (time_t) -1)
		return;

	tm = localtime (&tw->tw_clock);
	if (tm->tm_isdst) {
		tw->tw_flags |= TW_DST;
		tw->tw_zone -= 60;
	}
}
#endif /* ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST */
%}

sun	([Ss][Uu][Nn]([Dd][Aa][Yy])?)
mon	([Mm][Oo][Nn]([Dd][Aa][Yy])?)
tue	([Tt][Uu][Ee]([Ss][Dd][Aa][Yy])?)
wed	([Ww][Ee][Dd]([Nn][Ee][Ss][Dd][Aa][Yy])?)
thu	([Tt][Hh][Uu]([Rr][Ss][Dd][Aa][Yy])?)
fri	([Ff][Rr][Ii]([Dd][Aa][Yy])?)
sat	([Ss][Aa][Tt]([Uu][Rr][Dd][Aa][Yy])?)

DAY	({sun}|{mon}|{tue}|{wed}|{thu}|{fri}|{sat})

jan	([Jj][Aa][Nn]([Uu][Aa][Rr][Yy])?)
feb	([Ff][Ee][Bb]([Rr][Uu][Aa][Rr][Yy])?)
mar	([Mm][Aa][Rr]([Cc][Hh])?)
apr	([Aa][Pp][Rr]([Ii][Ll])?)
may	([Mm][Aa][Yy])
jun	([Jj][Uu][Nn]([Ee])?)
jul	([Jj][Uu][Ll]([Yy])?)
aug	([Aa][Uu][Gg]([Uu][Ss][Tt])?)
sep	([Ss][Ee][Pp]([Tt][Ee][Mm][Bb][Ee][Rr])?)
oct	([Oo][Cc][Tt]([Oo][Bb][Ee][Rr])?)
nov	([Nn][Oo][Vv]([Ee][Mm][Bb][Ee][Rr])?)
dec	([Dd][Ee][Cc]([Ee][Mm][Bb][Ee][Rr])?)

MONTH	({jan}|{feb}|{mar}|{apr}|{may}|{jun}|{jul}|{aug}|{sep}|{oct}|{nov}|{dec})

TIME	({D}:{d}{d}(:{d}{d})?)

/*
** The year can either be 2 digits, or 4. However, after
** Y2K, we found that some MUA were reporting the year 100, hence
** the middle term here. yyterminate() resolves the actual
** issues with 2-digit years.
*/

YEAR	(({d}{d})|(1{d}{d})|({d}{4}))

w	([ \t]*)
W	([ \t]+)
D	([0-9]?[0-9])
d	[0-9]
nl	[ \t\n()]

%%
%{
	/*
	** This section begins the definition of dparsetime().
	** Put here any local variable definitions and initializations
	*/
	YY_BUFFER_STATE lexhandle;

	unsigned char *cp;
	static struct tws tw;

	memset(&tw,0,sizeof(struct tws));

	lexhandle = yy_scan_string(lexstr);
%}

{DAY}","?{W}{MONTH}{W}{D}{W}{TIME}{W}{YEAR}  {
	INIT();
	SETWDAY();
	SKIPTOA();
	SETMON();
	SKIPTOD();
	SETDAY();
	SKIPTOD();
	SETTIME();
	SKIPTOD();
	SETYEAR();
}

{DAY}","?{W}{D}{W}{MONTH}{W}{YEAR}{W}{TIME}  {
	INIT();
	SETWDAY();
	SKIPTOD();
	SETDAY();
	SKIPTOA();
	SETMON();
	SKIPTOD();
	SETYEAR();
	SKIPTOD();
	SETTIME();
}
{D}{W}{MONTH}{W}{YEAR}{W}{TIME}  {
	INIT();
	SETDAY();
	SKIPTOA();
	SETMON();
	SKIPTOD();
	SETYEAR();
	SKIPTOD();
	SETTIME();
}
{DAY}","?{W}{MONTH}{W}{D}","?{W}{YEAR}","?{W}{TIME}  {
	INIT();
	SETWDAY();
	SKIPTOA();
	SETMON();
	SKIPTOD();
	SETDAY();
	SKIPTOD();
	SETYEAR();
	SKIPTOD();
	SETTIME();
}
{DAY}","?{W}{MONTH}{W}{D}","?{W}{YEAR}  {
	INIT();
	SETWDAY();
	SKIPTOA();
	SETMON();
	SKIPTOD();
	SETDAY();
	SKIPTOD();
	SETYEAR();
}
{MONTH}{W}{D}","?{W}{YEAR}","?{W}{DAY}  {
	INIT();
	SETMON();
	SKIPTOD();
	SETDAY();
	SKIPTOD();
	SETYEAR();
	SKIPTOA();
	SETWDAY();
}
{MONTH}{W}{D}","?{W}{YEAR}  {
	INIT();
	SETMON();
	SKIPTOD();
	SETDAY();
	SKIPTOD();
	SETYEAR();
}
{d}{4}"-"{d}{2}"-"{d}{2}(" "|"T"){TIME}  {
	INIT();
	SETYEAR();
	SKIPTOD();
	SETMON_NUM();
	SKIPTOD();
	SETDAY();
	SKIPTOD();
	SETTIME();
}
{d}{4}"-"{d}{2}"-"{d}{2}  {
	INIT();
	SETYEAR();
	SKIPTOD();
	SETMON_NUM();
	SKIPTOD();
	SETDAY();
}
{d}{2}"-"{d}{2}"-"{d}{2}  {
	fprintf(stderr, "the highly ambiguous date format XX-XX-XX..."
			" is no longer supported\n");
}
{D}"/"{D}"/"{YEAR}{W}{TIME}  {
	INIT();
	if(europeandate) {
		/* DD/MM/YY */
		SETDAY();
		SKIPTOD();
		SETMON_NUM();
	} else {
		/* MM/DD/YY */
		SETMON_NUM();
		SKIPTOD();
		SETDAY();
	}
	SKIPTOD();
	SETYEAR();
	SKIPTOD();
	SETTIME();
}
{D}"/"{D}"/"{YEAR}  {
	INIT();
	if(europeandate) {
		/* DD/MM/YY */
		SETDAY();
		SKIPTOD();
		SETMON_NUM();
	} else {
		/* MM/DD/YY */
		SETMON_NUM();
		SKIPTOD();
		SETDAY();
	}
	SKIPTOD();
	SETYEAR();
}

"[Aa][Mm]"
"[Pp][Mm]"  tw.tw_hour += 12;

"+"{D}{d}{d}  {
	INIT();
	SKIPTOD();
	SETZONE(atoi(cp));
#ifdef ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST
	zonehack (&tw);
#endif /* ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST */
	yyterminate();
}
"-"{D}{d}{d}  {
	INIT();
	SKIPTOD();
	SETZONE(-atoi(cp));
#ifdef ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST
	zonehack (&tw);
#endif /* ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST */
	yyterminate();

}
"+"{d}{d}":"{d}{d}  {
	INIT();
	SKIPTOD();
	SETZONEC(atoi(cp), atoi(cp+3));
#ifdef ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST
	zonehack (&tw);
#endif /* ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST */
	yyterminate();
}
"-"{d}{d}":"{d}{d}  {
	INIT();
	SKIPTOD();
	SETZONEC(-atoi(cp), -atoi(cp+3));
#ifdef ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST
	zonehack (&tw);
#endif /* ADJUST_NUMERIC_ONLY_TZ_OFFSETS_WRT_DST */
	yyterminate();

}
{nl}("ut"|"UT")		INIT(); SETZONE(0); yyterminate();
{nl}("gmt"|"GMT")	INIT(); SETZONE(0); yyterminate();
{nl}("est"|"EST")	INIT(); SETZONE(-500); yyterminate();
{nl}("edt"|"EDT")	{ INIT(); SETDST(); SETZONE(-500); yyterminate(); }
{nl}("cst"|"CST")	INIT(); SETZONE(-600); yyterminate();
{nl}("cdt"|"CDT")	{ INIT(); SETDST(); SETZONE(-600); yyterminate(); }
{nl}("mst"|"MST")	INIT(); SETZONE(-700); yyterminate();
{nl}("mdt"|"MDT")	{ INIT(); SETDST(); SETZONE(-700); yyterminate(); }
{nl}("pst"|"PST")	INIT(); SETZONE(-800); yyterminate();
{nl}("pdt"|"PDT")	{ INIT(); SETDST(); SETZONE(-800); yyterminate(); }
{nl}("nst"|"NST")	INIT(); SETZONE(-330); yyterminate();
{nl}("ast"|"AST")	INIT(); SETZONE(-400); yyterminate();
{nl}("adt"|"ADT")	{ INIT(); SETDST(); SETZONE(-400); yyterminate(); }
{nl}("hst"|"HST")	INIT(); SETZONE(-1000); yyterminate();
{nl}("hdt"|"HDT")	{ INIT(); SETDST(); SETZONE(-1000); yyterminate(); }
.|\n

%%
/*
** This is a portable way to squash a warning about the yyunput()
** function being static but never used. It costs us a tiny amount
** of extra code in the binary but the other options are:
** "%option nounput" which is flex-specific
** makefile hackery just to compile dtimep.c with different flags
*/
void dtimep_yyunput(int c)
{
	unput(c);
}
