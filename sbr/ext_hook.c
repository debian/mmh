/*
** ext_hook.c -- Run a program that hooks into some other system.
*/

#include <h/mh.h>

/*
** The filename arguments are given full path names.
** msg_filename2 might contain a NULL pointer if not needed.
** Look in the context for an error message if something goes wrong;
** there is a built-in message in case one isn't specified.
** Only produces the error message once.
*/
int
ext_hook(char *hook_name, char *msg_filename1, char *msg_filename2)
{
	char *hook;  /* hook program from context */
	int status;

	static int did_message = 0;  /* we've already output a message */

	if (!(hook = context_find(hook_name))) {
		return (OK);
	}
	status = execprogl(mhbasename(hook), mhbasename(hook),
			msg_filename1, msg_filename2,
			(char *)NULL);
	if (status != OK) {
		if (did_message) {
			return (NOTOK);
		}
		advise(NULL, "external hook (%s) failed.", hook);
		did_message = 1;
		return (NOTOK);
	} else {
		return (OK);
	}
}
