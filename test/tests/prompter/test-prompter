#!/bin/sh
######################################################
#
# Test prompter
#
######################################################


. "$MH_TEST_COMMON"



# check -help
runandcheck "prompter -help" <<!
Usage: prompter [switches] file
  switches are:
  -[no]prepend
  -[no]rapid
  -[no]body
  -Version
  -help
!


# check -version
case `prompter -V` in
  prompter\ --*) ;;
  *            ) printf '%s: prompter -v generated unexpected output\n' "$0" >&2
                 failed=`expr ${failed:-0} + 1`;;
esac

# check unknown switch
runandcheck 'prompter -nonexistent' <<!
prompter: -nonexistent unknown
!



# check with no switches

runandcheck 'prompter' <<!
prompter: usage: prompter [switches] file
!



# check with file

cat >$MH_TEST_DIR/prompter-file <<EOF
Resent-From: sender@example.com
Resent-To:
Resent-cc:
Resent-fcc:
EOF

printf 'recipient@example.com\ncc@example.com\n+outbox\nmessage body\n' | \
	prompter $MH_TEST_DIR/prompter-file >/dev/null

runandcheck "cat $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------
message body
!



# check -noprepend

cat >$MH_TEST_DIR/prompter-file <<EOF
Resent-From: sender@example.com
Resent-To:
Resent-cc:
Resent-fcc:
--------
message body
EOF

printf 'recipient@example.com\ncc@example.com\n+outbox\nappendage\n' | \
	prompter -noprepend $MH_TEST_DIR/prompter-file >/dev/null

runandcheck "cat $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------
message body
appendage
!


# check -prepend

cat >$MH_TEST_DIR/prompter-file <<EOF
Resent-From: sender@example.com
Resent-To:
Resent-cc:
Resent-fcc:
--------
message body
EOF

printf 'recipient@example.com\ncc@example.com\n+outbox\nprependage\n' | \
	prompter -noprepend -prepend $MH_TEST_DIR/prompter-file >/dev/null

runandcheck "cat $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------
prependage
message body
!


# check -rapid

runandcheck "</dev/null prompter -rapid $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------Enter initial text
--------
!



# check -norapid

runandcheck "</dev/null prompter -rapid -norapid $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------Enter initial text
prependage
message body
--------
!



# check -body.

runandcheck "echo 'woot woot' | prompter -nobody -body $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------Enter initial text
prependage
message body
--------
!

runandcheck "cat $MH_TEST_DIR/prompter-file" <<!
Resent-From: sender@example.com
Resent-To: recipient@example.com
Resent-cc: cc@example.com
Resent-fcc: +outbox
--------
woot woot
prependage
message body
!
