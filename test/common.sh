# Common helper routines for test shell scripts
# -- intended to be sourced by them

tmpfile="`mktemp`"
trap '
	rm -f "$tmpfile"
	exit "$failed"
' 0 1 2 15
failed=0


test_skip()
{
	echo "$0: $1"
	failed=120
	exit
}

# portable implementation of 'which' utility
findprog()
{
	FOUND=
	PROG="$1"
	IFS_SAVE="$IFS"
	IFS=:
	for D in $PATH; do
		if [ -z "$D" ]; then
			D=.
		fi
		if [ -f "$D/$PROG" ] && [ -x "$D/$PROG" ]; then
			printf '%s\n' "$D/$PROG"
			break
		fi
	done
	IFS="$IFS_SAVE"
}

require_prog()
{
	if [ -z "$(findprog $1)" ]; then
		test_skip "missing $1"
	fi
}

require_locale()
{
	for locale in "$@"; do
		if locale -a | grep -i "$locale" >/dev/null; then
			return
		fi
	done
	test_skip "no suitable locale available"
}

# Do a best guess at FQDN
mh_hostname()
{
	hostname -f 2>/dev/null || uname -n
}

# Some stuff for doing silly progress indicators
progress_update()
{
	test -t 1 || return 0   # suppress progress meter if non-interactive
	this="$1"
	first="$2"
	last="$3"
	range="$(expr $last - $first ||:)"
	prog="$(expr $this - $first ||:)"
	# this automatically rounds to nearest integer
	perc="$(expr 100 \* $prog / $range ||:)"
	# note \r so next update will overwrite
	printf "%3d%%\r" $perc
}

progress_done()
{
	test -t 1 || return 0   # suppress progress meter if non-interactive
	printf "100%%\n"
}


#### Replace generated Content-ID headers with static value
replace_contentid()
{
	sed "/^Content-ID/s/:.*/: <TESTID>/" "$@"
}


#### Filter that squeezes blank lines, partially emulating GNU cat -s,
#### but sufficient for our purpose.
#### From http://www-rohan.sdsu.edu/doc/sed.html, compiled by Eric Pement.
squeeze_lines()
{
	sed '/^$/N;/\n$/D'
}

#### Filter that converts non-breakable space U+00A0 to an ASCII space.
prepare_space()
{
	sed 's/'"`printf '\\302\\240'`"'/ /g'
}


# first argument:	command line to run
# second argument:	"normspace" to normalize the whitespace
# stdin:		expected output
runandcheck()
{
	if [ "$2" = "normspace" ]; then
		eval "$1" 2>&1 | prepare_space >"$tmpfile"
		diff="`prepare_space | diff -ub - "$tmpfile" || :`"
	else
		eval "$1" >"$tmpfile" 2>&1
		diff="`diff -u - "$tmpfile"`"
	fi

	if [ "$diff" ]; then
		echo "$0: $1 failed"
		echo "$diff"
		failed=`expr "${failed:-0}" + 1`
	fi
}
