/*
** config.c -- master nmh configuration file
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>



/*
** nmh globals
*/

char *mhetcdir = NMHETCDIR;

char *invo_name;        /* command invocation name    */
char *mypath;           /* user's $HOME               */
char *mmhpath;          /* pathname of user's mmh dir */
char *defpath;          /* pathname of user's profile */
char *ctxpath;          /* pathname of user's context */
char ctxflags;          /* status of user's context   */
struct node *m_defs;    /* profile/context structure  */


/*
** nmh constants: standard file names
**
** Important: Adjust uip/mmh.sh if you make changes here!
*/

/* default name of the mail storage */
char *mailstore = "Mail";

/* default name of user profile */
char *mmhdir = ".mmh";

/* default name of user profile */
char *profile = "profile";

/* default name for the nmh context file */
char *context = "context";

/*
** Default name of file for public sequences. Gets overridden
** by a `Mh-Sequences' entry in the user's profile. Set to NULL
** or the empty string to use private sequences by default.
*/
char *mh_seq = ".mh_sequences";

/* standard component files */
char *components     = "components";       /* comp         */
char *replcomps      = "replcomps";        /* repl         */
char *replgroupcomps = "replgroupcomps";   /* repl -group  */
char *forwcomps      = "forwcomps";        /* forw         */
char *distcomps      = "distcomps";        /* dist         */
char *rcvdistcomps   = "rcvdistcomps";     /* rcvdist      */
char *digestcomps    = "digestcomps";      /* forw -digest */

/* standard format (filter) files */
char *mhlformat      = "mhl.format";       /* show         */
char *mhlreply       = "mhl.reply";        /* repl         */
char *scanformat     = "scan.default";     /* scan/inc     */


/*
** standard names for: mail folders, sequences, and profile entries
*/

/* some default folder names */
char *defaultfolder = "+inbox";
char *draftfolder = "+drafts";
char *trashfolder = "+trash";

char *inbox = "Inbox";  /* profile entry name to specify the default folder */
char *curfolder = "Current-Folder";

/* predefined sequences */
char *seq_all    = "a";
char *seq_beyond = "b";  /* the previous `new' sequence */
char *seq_cur    = "c";
char *seq_first  = "f";
char *seq_last   = "l";
char *seq_next   = "n";
char *seq_prev   = "p";
char *seq_unseen = "u";
char *seq_neg    = "!";

char *usequence = "Unseen-Sequence";
char *psequence = "Previous-Sequence";
char *nsequence = "Sequence-Negation";

/* profile entries for storage locations */
char *nmhstorage   = "nmh-storage";

/* Default header field names */
char *attach_hdr = "Attach";
char *sign_hdr = "Sign";
char *enc_hdr = "Enc";

/* the tool to query the mime type of a file */
char *mimetypequery = "Mime-Type-Query";
char *mimetypequeryproc = "file -b --mime";



/*
** nmh default programs
*/

/*
** This is the default program invoked by a "list" or "display" response
** at the "What now?" prompt. It will be given the absolute pathname of
** the message to show.
*/
char *listproc = "show";

/*
** This is used by mhl as a front-end.  It is also used
** by show(1) as the default method of displaying message bodies
** or message parts of type text/plain.
*/
char *defaultpager = "more";

/*
** This is the editor invoked by the various message
** composition programs.  It SHOULD be a full screen
** editor, such as vi or emacs, but any editor will work.
*/
char *defaulteditor = "vi";

/*
** This program is called after comp, et. al., have built a draft
*/
char *whatnowproc = "whatnow";

/*
** This is the sendmail interface to use for sending mail.
*/
char *sendmail = SENDMAILPATH;

/*
** This is the path to the system mail spool directory (e.g. `/var/mail').
*/
char *mailspool = MAILSPOOL;


/*
** file stuff
*/

/*
** Folders (directories) are created with this protection (mode)
*/
char *foldprot = "0700";

/*
** Every NEW message will be created with this protection.  When a
** message is filed it retains its protection.
*/
char *msgprot = "0600";



/*
** Standard yes/no switches structure
*/
struct swit anoyes[] = {
	{ "no", 0 },
	{ "yes", 0 },
	{ NULL, 0 }
};

