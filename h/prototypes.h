/*
** prototypes.h -- various prototypes
*/

/*
** missing system prototypes
*/
#ifndef HAVE_TERMCAP_H
extern int tgetent(char *bp, char *name);
extern int tgetnum(char *id);
extern int tgetflag(char *id);
extern char *tgetstr(char *id, char **area);
extern char *tgoto(char *cm, int destcol, int destline);
extern int tputs(char *cp, int affcnt, int (*outc) (int));
#endif

/*
** prototype from config.h
*/
char *etcpath(char *);

/*
** prototypes from the nmh subroutine library
*/
void adios(int, char *, char *, ...) NORETURN;
void admonish(char *, char *, ...);
void advertise(char *, char *, char *, va_list);
void advise(char *, char *, ...);
void ambigsw(char *, struct swit *);
char **brkstring(char *, char *, char *);
int is_native_charset(char *);
char *concat(char *, ...);
int context_del(char *);
char *context_find(char *);
void context_read(void);
void context_replace(char *, char *);
void context_save(void);
void cpydata(int, int, char *, char *);
void cpydgst(int, int, char *, char *);
int decode_rfc2047(char *, char *, size_t);
int default_done(int);
int encode_rfc2047(const char *name, char **value, const char *charset);
int execprog(char *, char **);
int execprogl(char *, char *, ...);
char *expandfol(char *);
char *expanddir(char *);
int ext_hook(char *, char *, char *);
int folder_addmsg(struct msgs **, char *, int, int, int, int, char *);
int folder_delmsgs(struct msgs *, int);
void folder_free(struct msgs *);
struct msgs *folder_read(char *);
struct msgs *folder_realloc(struct msgs *, int, int);
int gans(char *, struct swit *);
char **getans(char *, struct swit *);
int getanswer(char *);
char **getarguments(char *, int, char **, int);
char *get_charset();
char *getcurfol(void);
char *getdeffol(void);
int lkclose(int, char*);
int lkfclose(FILE *, char *);
FILE *lkfopen(char *, char *);
int lkopen(char *, int, mode_t);
int m_atoi(char *);
char *m_backup(char *);
int m_convert(struct msgs *, char *);
char *m_draft(char *);
enum state m_getfld2(enum state, struct field *, FILE *);
int m_gmprot(void);
char *m_name(int);
int m_putenv(char *, char *);
char *m_mktemp(const char *, int *, FILE **);
char *m_mktemp2(const char *, const char *, int *, FILE **);
void thisisanmbox(FILE *);
int makedir(char *);
int mh_strcasecmp(const char *s1, const char *s2);
char *norm_charmap(char *);
char *new_fs(char *, char *);
int pidwait(pid_t, int);
int pidstatus(int, FILE *, char *);
int pidXwait(int, char *);
void print_help(char *, struct swit *, int);
void print_sw(char *, struct swit *, char *, FILE *);
void print_version(char *);
void push(void);
char *pwd(void);
char *mhbasename(char *);
void readconfig(struct node **, FILE *, char *, int);
int seq_addmsg(struct msgs *, char *, int, int, int);
int seq_addsel(struct msgs *, char *, int, int);
char *seq_bits(struct msgs *);
int seq_delmsg(struct msgs *, char *, int);
int seq_delsel(struct msgs *, char *, int, int);
int seq_getnum(struct msgs *, char *);
char *seq_list(struct msgs *, char *);
int seq_nameok(unsigned char *);
void seq_print(struct msgs *, char *);
void seq_printall(struct msgs *);
void seq_read(struct msgs *);
void seq_save(struct msgs *);
void seq_setcur(struct msgs *, int);
void seq_setprev(struct msgs *);
void seq_setunseen(struct msgs *, int);
int showfile(char **, char *);
int smatch(char *, struct swit *);
char *snprintb(char *, size_t, unsigned, char *);
int stringdex(char *, char *);
char *toabsdir(char *);
char *trim(unsigned char *);
char *rtrim(char *);
char *trimcpy(unsigned char *);
int unputenv(char *);
void unquote_string(const char *input, char *output);
int uprf(char *, char *);
int vfgets(FILE *, char **);
char *write_charset_8bit(void);




/*
** prototypes for some routines in uip
*/
int distout(char *, char *, char *);
int what_now(char *, int, char *, char *, int, struct msgs *,
		char *, char *);

/*
** from the former mts.h
*/
char *LocalName(void);  /* hostname */
char *getusername(void);
char *getfullname(void);


/*
** prototypes for message and sequence manipulation
*/
void clear_msg_flags(struct msgs *, int);
void copy_msg_flags(struct msgs *, int, int);
void get_msg_flags(struct msgs *, seqset_t *, int);
void set_msg_flags(struct msgs *, seqset_t *, int);
seqset_t does_exist(struct msgs *, int);
void unset_exists(struct msgs *, int);
void set_exists(struct msgs *, int);
seqset_t is_selected(struct msgs *, int);
void unset_selected(struct msgs *, int);
void set_selected(struct msgs *, int);
seqset_t is_unseen(struct msgs *, int);
void unset_unseen(struct msgs *, int);
void set_unseen(struct msgs *, int);
seqset_t in_sequence(struct msgs *, int, int);
void clear_sequence(struct msgs *, int, int);
void add_sequence(struct msgs *, int, int);

int is_seq_private(struct msgs *, int);
void make_seq_public(struct msgs *, int);
void make_seq_private(struct msgs *, int);
void make_all_public(struct msgs *);

void clear_folder_flags(struct msgs *);
int is_readonly(struct msgs *);
void set_readonly(struct msgs *);
int other_files(struct msgs *);
void set_other_files(struct msgs *);
char *getthreadid(const char *);
