/*
** scansbr.h -- definitions for scan()
*/

extern char *scanl;

#define SCNMSG  1     /* message just fine                    */
#define SCNEOF  0     /* empty message                        */
#define SCNERR  (-1)  /* error message                        */
#define SCNNUM  (-2)  /* number out of range                  */
#define SCNFAT  (-3)  /* fatal error                          */

#define WIDTH  78

#define SCN_MBOX (-1)
#define SCN_FOLD 0

/*
** prototypes
*/
int scan(FILE *, int, int, char *, int, int, int);
