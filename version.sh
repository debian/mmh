#!/bin/sh
#
# version.sh -- script to create version string(s) for mmh.
#
# You can pass the top soucre directory to the script.
#

if [ -d "$1" ]
then
	cd "$1"
fi

if [ ! -f VERSION ]
then
	echo "No version file found" 1>&2
	echo "usage: $0 [mmh-sourcedir]" 1>&2
	exit 1
fi

version="`sed q VERSION`"

git_info=""

if [ -d ".git" ]; then
	current=`git log -n 1 --pretty=format:+%h HEAD`
	release=`git log -n 1 --pretty=format:+%h "mmh-$version"`
	if [ "$current" != "$release" ]
	then
		git_info="$current"
	fi
fi

echo mmh-"$version""$git_info"
