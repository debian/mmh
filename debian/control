Source: mmh
Section: mail
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 12),
 bash-completion,
 flex,
 libncurses-dev,
 dpkg-dev (>= 1.19.0.5),
Standards-Version: 4.6.2
Homepage: http://marmaro.de/prog/mmh/
Vcs-Browser: https://salsa.debian.org/debian/mmh
Vcs-Git: https://salsa.debian.org/debian/mmh.git
Rules-Requires-Root: no

Package: mmh
Architecture: any
Replaces: mh
Provides: mail-reader, mh
Conflicts: mh
Depends: file, ${misc:Depends}, ${shlibs:Depends}
Description: set of electronic mail handling programs
 This is the mmh mail user agent (reader/sender), a command-line based mail
 reader that is powerful and extensible.  mmh is an excellent choice for
 people who receive and process a lot of mail.
 .
 Unlike most mail user agents, mmh is not a single program, rather it is a
 set of programs that are run from the shell.  This allows the user to
 utilize the full power of the Unix shell in coordination with mmh.
 .
 Mmh is a modified version of the electronic mail handling system nmh.
 Nmh (new MH) itself was originally based on the package MH-6.8.3, and
 was intended to be a (mostly) compatible drop-in replacement for MH.
 In contrast, mmh is not intended to be a drop-in replacement for nmh,
 rather mmh breaks compatibility to nmh in order to modernize and
 simplify it.
