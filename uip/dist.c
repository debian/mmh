/*
** dist.c -- re-distribute a message
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/utils.h>
#include <fcntl.h>
#include <unistd.h>
#include <locale.h>
#include <sysexits.h>

static struct swit switches[] = {
#define ANNOSW  0
	{ "annotate", 0 },
#define NANNOSW  1
	{ "noannotate", 2 },
#define EDITRSW  2
	{ "editor editor", 0 },
#define FORMSW  3
	{ "form formfile", 0 },
#define WHATSW  4
	{ "whatnowproc program", 0 },
#define VERSIONSW  5
	{ "Version", 0 },
#define HELPSW  6
	{ "help", 0 },
	{ NULL, 0 }
};

char *version=VERSION;

int
main(int argc, char **argv)
{
	int anot = 0;
	int in, out;
	char *cp, *cwd, *maildir, *msgnam;
	char *ed = NULL, *folder = NULL;
	char *form = NULL, *msg = NULL, buf[BUFSIZ], drft[BUFSIZ];
	char **argp, **arguments;
	struct msgs *mp = NULL;
	char *fmtstr;

	setlocale(LC_ALL, "");
	invo_name = mhbasename(argv[0]);

	/* read user profile/context */
	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf), "%s [+folder] [msg] [switches]", invo_name);
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case ANNOSW:
				anot++;
				continue;
			case NANNOSW:
				anot = 0;
				continue;

			case EDITRSW:
				if (!(ed = *argp++) || *ed == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				continue;

			case WHATSW:
				if (!(whatnowproc = *argp++) || *whatnowproc == '-') {
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				}
				continue;

			case FORMSW:
				if (!(form = *argp++) || *form == '-') {
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				}
				continue;
			}
		}
		if (*cp == '+' || *cp == '@') {
			if (folder) {
				adios(EX_USAGE, NULL, "only one folder at a time!");
			} else {
				folder = mh_xstrdup(expandfol(cp));
			}
		} else {
			if (msg) {
				adios(EX_USAGE, NULL, "only one message at a time!");
			} else {
				msg = cp;
			}
		}
	}

	cwd = mh_xstrdup(pwd());

	strncpy(drft, m_draft(seq_beyond), sizeof(drft));
	if ((out = creat(drft, m_gmprot())) == NOTOK) {
		adios(EX_CANTCREAT, drft, "unable to create");
	}

	fmtstr = new_fs(form, distcomps);
	if (write(out, fmtstr, strlen(fmtstr)) != (int)strlen(fmtstr)) {
		adios(EX_IOERR, drft, "error writing");
	}
	close(out);

	if (!msg) {
		msg = seq_cur;
	}
	if (!folder) {
		folder = getcurfol();
	}
	maildir = toabsdir(folder);

	if (chdir(maildir) == NOTOK) {
		adios(EX_OSERR, maildir, "unable to change directory to");
	}

	if (!(mp = folder_read(folder))) {
		adios(EX_IOERR, NULL, "unable to read folder %s", folder);
	}

	/* check for empty folder */
	if (mp->nummsg == 0) {
		adios(EX_NOINPUT, NULL, "no messages in %s", folder);
	}

	/* parse the message range/sequence/name and set SELECTED */
	if (!m_convert(mp, msg)) {
		exit(EX_USAGE);
	}
	seq_setprev(mp);

	if (mp->numsel > 1) {
		adios(EX_USAGE, NULL, "only one message at a time!");
	}

	msgnam = mh_xstrdup(m_name(mp->lowsel));
	if ((in = open(msgnam, O_RDONLY)) == NOTOK) {
		adios(EX_IOERR, msgnam, "unable to open message");
	}

	context_replace(curfolder, folder);
	seq_setcur(mp, mp->lowsel);
	seq_save(mp);  /* synchronize sequences  */
	context_save();

	what_now(ed, NOUSE, drft, msgnam, 1, mp, anot ? "Resent" : NULL, cwd);
	return EX_SOFTWARE;
}
