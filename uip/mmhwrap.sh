#!/bin/sh
#
# Wrapper for mmh commands
#
# For parallel installations of nmh and mmh.
# Removes the need to add /usr/local/mmh/bin to $PATH.
#
# Copy only this script into a $PATH directory.

PATH="%bindir%:$PATH"
exec "$@"
