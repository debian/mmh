/*
** mhshow.c -- display the contents of MIME messages
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <fcntl.h>
#include <h/signals.h>
#include <errno.h>
#include <signal.h>
#include <h/tws.h>
#include <h/mime.h>
#include <h/mhparse.h>
#include <h/utils.h>
#include <unistd.h>
#include <sys/stat.h>
#include <locale.h>
#include <sysexits.h>

static struct swit switches[] = {
#define VERBSW  0
	{ "verbose", 0 },
#define NVERBSW  1
	{ "noverbose", 2 },
#define FILESW  2  /* interface from show */
	{ "file file", 0 },
#define FORMSW  3
	{ "form formfile", 0 },
#define PARTSW  4
	{ "part number", 0 },
#define TYPESW  5
	{ "type content", 0 },
#define VERSIONSW  6
	{ "Version", 0 },
#define HELPSW  7
	{ "help", 0 },
#define DEBUGSW  8
	{ "debug", -5 },
	{ NULL, 0 }
};

char *version=VERSION;

/* mhparse.c */
extern char *tmp;  /* directory to place temp files */

/* mhshowsbr.c */
extern int nolist;
extern char *formsw;

/* mhmisc.c */
extern int npart;
extern int ntype;
extern char *parts[NPARTS + 1];
extern char *types[NTYPES + 1];
extern int userrs;

static enum { SHOW, NEXT, PREV } mode = SHOW;

int debugsw = 0;
int verbosw = 0;

#define quitser pipeser

/* mhparse.c */
CT parse_mime(char *);

/* mhmisc.c */
int part_ok(CT, int);
int type_ok(CT, int);
void set_endian(void);
void flush_errors(void);

/* mhshowsbr.c */
void show_all_messages(CT *);

/* mhfree.c */
void free_content(CT);
extern CT *cts;
void freects_done();

/*
** static prototypes
*/
static void pipeser(int);
static void m_popen(char *);
static void m_pclose(void);


int
main(int argc, char **argv)
{
	int msgnum;
	char *cp, *file = NULL, *folder = NULL;
	char *maildir, buf[100], **argp;
	char **arguments;
	struct msgs_array msgs = { 0, 0, NULL };
	struct msgs *mp = NULL;
	CT ct, *ctp;
	FILE *fp;
	int ontty = 0;

	if (atexit(freects_done) != 0) {
		adios(EX_OSERR, NULL, "atexit failed");
	}

	setlocale(LC_ALL, "");
	invo_name = mhbasename(argv[0]);
	if (mh_strcasecmp(invo_name, "next")==0) {
		mode = NEXT;
	} else if (mh_strcasecmp(invo_name, "prev")==0) {
		mode = PREV;
	}

	/* read user profile/context */
	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	/*
	** Parse arguments
	*/
	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf), "%s [+folder] %s[switches]", invo_name, mode==SHOW ? "[msgs] " : "");
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case PARTSW:
				if (!(cp = *argp++) || *cp == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				if (npart >= NPARTS)
					adios(EX_USAGE, NULL, "too many parts (starting with %s), %d max", cp, NPARTS);
				parts[npart++] = cp;
				continue;

			case TYPESW:
				if (!(cp = *argp++) || *cp == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				if (ntype >= NTYPES)
					adios(EX_USAGE, NULL, "too many types (starting with %s), %d max", cp, NTYPES);
				types[ntype++] = cp;
				continue;

			case FILESW:
				if (mode != SHOW) {
					adios(EX_USAGE, NULL, "Either call show as `%s' or use -file", invo_name);
				}

				if (!(cp = *argp++) || (*cp == '-' && cp[1]))
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				file = *cp == '-' ? cp : mh_xstrdup(expanddir(cp));
				continue;

			case FORMSW:
				if (!(cp = *argp++) || *cp == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				if (formsw)
					mh_free0(&formsw);
				formsw = mh_xstrdup(etcpath(cp));
				continue;

			case VERBSW:
				verbosw = 1;
				continue;
			case NVERBSW:
				verbosw = 0;
				continue;
			case DEBUGSW:
				debugsw = 1;
				continue;
			}
		}
		if (*cp == '+' || *cp == '@') {
			if (folder)
				adios(EX_USAGE, NULL, "only one folder at a time!");
			else
				folder = mh_xstrdup(expandfol(cp));
		} else if (mode != SHOW) {
			adios(EX_USAGE, NULL, "Either call show as `%s' or give message arguments", invo_name);
		} else {
			app_msgarg(&msgs, cp);
		}
	}

	/* null terminate the list of acceptable parts/types */
	parts[npart] = NULL;
	types[ntype] = NULL;

	set_endian();

	if ((cp = getenv("MM_NOASK")) && strcmp(cp, "1")==0) {
		nolist  = 1;
	}

	/*
	** Check if we've specified an additional profile
	*/
	if ((cp = getenv("MHSHOW"))) {
		if ((fp = fopen(cp, "r"))) {
			readconfig((struct node **) 0, fp, cp, 0);
			fclose(fp);
		} else {
			admonish("", "unable to read $MHSHOW profile (%s)",
					cp);
		}
	}

	/*
	** Read the standard profile setup
	*/
	if ((fp = fopen(cp = etcpath("mhn.defaults"), "r"))) {
		readconfig((struct node **) 0, fp, cp, 0);
		fclose(fp);
	}

	/*
	** Check for storage directory.  If specified,
	** then store temporary files there.  Else we
	** store them in standard nmh directory.
	*/
	if ((cp = context_find(nmhstorage)) && *cp)
		tmp = concat(cp, "/", invo_name, NULL);
	else
		tmp = mh_xstrdup(toabsdir(invo_name));

	if (file && msgs.size)
		adios(EX_USAGE, NULL, "cannot specify msg and file at same time!");

	/*
	** check if message is coming from file
	*/
	if (file) {
		cts = mh_xcalloc(2, sizeof(*cts));
		ctp = cts;

		if ((ct = parse_mime(file)))
			*ctp++ = ct;
	} else {
		/*
		** message(s) are coming from a folder
		*/
		if (!msgs.size) {
			switch (mode) {
			case NEXT:
				app_msgarg(&msgs, seq_next);
				break;
			case PREV:
				app_msgarg(&msgs, seq_prev);
				break;
			default:
				app_msgarg(&msgs, seq_cur);
				break;
			}
		}
		if (!folder)
			folder = getcurfol();
		maildir = toabsdir(folder);

		if (chdir(maildir) == NOTOK)
			adios(EX_OSERR, maildir, "unable to change directory to");

		/* read folder and create message structure */
		if (!(mp = folder_read(folder)))
			adios(EX_IOERR, NULL, "unable to read folder %s", folder);

		/* check for empty folder */
		if (mp->nummsg == 0)
			adios(EX_DATAERR, NULL, "no messages in %s", folder);

		/* parse all the message ranges/sequences and set SELECTED */
		for (msgnum = 0; msgnum < msgs.size; msgnum++)
			if (!m_convert(mp, msgs.msgs[msgnum]))
				exit(EX_USAGE);

		/*
		** Set the SELECT_UNSEEN bit for all the SELECTED messages,
		** since we will use that as a tag to know which messages
		** to remove from the "unseen" sequence.
		*/
		for (msgnum = mp->lowsel; msgnum <= mp->hghsel; msgnum++)
			if (is_selected(mp, msgnum))
				set_unseen(mp, msgnum);

		seq_setprev(mp);  /* set the Previous-Sequence */
		seq_setunseen(mp, 0);  /* unset unseen seqs for shown msgs */

		cts = mh_xcalloc(mp->numsel + 1, sizeof(*cts));
		ctp = cts;

		/*
		** Parse all the SELECTED messages.
		*/
		for (msgnum = mp->lowsel; msgnum <= mp->hghsel; msgnum++) {
			if (is_selected(mp, msgnum)) {
				char *msgnam;

				msgnam = m_name(msgnum);
				if ((ct = parse_mime(msgnam)))
					*ctp++ = ct;
			}
		}
	}

	if (!*cts)
		exit(EX_SOFTWARE);

	userrs = 1;
	SIGNAL(SIGQUIT, quitser);
	SIGNAL(SIGPIPE, pipeser);

	/*
	** Get the associated umask for the relevant contents.
	*/
	for (ctp = cts; *ctp; ctp++) {
		struct stat st;

		ct = *ctp;
		if (type_ok(ct, 1) && !ct->c_umask) {
			if (stat(ct->c_file, &st) != NOTOK)
				ct->c_umask = ~(st.st_mode & 0777);
			else
				ct->c_umask = ~m_gmprot();
		}
	}

	if ((ontty = isatty(fileno(stdout)))) {
		m_popen(defaultpager);
	}

	/*
	** Show the message content
	*/
	show_all_messages(cts);

	if (ontty) {
		m_pclose();
	}

	/* Now free all the structures for the content */
	for (ctp = cts; *ctp; ctp++)
		free_content(*ctp);

	mh_free0(&cts);

	/* If reading from a folder, do some updating */
	if (mp) {
		context_replace(curfolder, folder); /* update current folder */
		seq_setcur(mp, mp->hghsel);        /* update current message */
		seq_save(mp);                      /* synchronize sequences */
		context_save();                    /* save the context file */
	}

	return 0;
}


static void
pipeser(int i)
{
	if (i == SIGQUIT) {
		unlink("core");
		fflush(stdout);
		fprintf(stderr, "\n");
		fflush(stderr);
	}

	_exit(EX_IOERR);
	/* NOTREACHED */
}


static int m_pid = NOTOK;
static int sd = NOTOK;


static void
m_popen(char *name)
{
	int pd[2];

	if ((sd = dup(fileno(stdout))) == NOTOK)
		adios(EX_OSERR, "standard output", "unable to dup()");

	if (pipe(pd) == NOTOK)
		adios(EX_OSERR, "pipe", "unable to");

	switch (m_pid = fork()) {
	case NOTOK:
		adios(EX_OSERR, "fork", "unable to");

	case OK:
		SIGNAL(SIGINT, SIG_DFL);
		SIGNAL(SIGQUIT, SIG_DFL);

		close(pd[1]);
		if (pd[0] != fileno(stdin)) {
			dup2(pd[0], fileno(stdin));
			close(pd[0]);
		}
		execlp(name, mhbasename(name), NULL);
		fprintf(stderr, "unable to exec ");
		perror(name);
		_exit(EX_OSERR);

	default:
		close(pd[0]);
		if (pd[1] != fileno(stdout)) {
			dup2(pd[1], fileno(stdout));
			close(pd[1]);
		}
	}
}


void
m_pclose(void)
{
	if (m_pid == NOTOK)
		return;

	if (sd != NOTOK) {
		fflush(stdout);
		if (dup2(sd, fileno(stdout)) == NOTOK)
			adios(EX_OSERR, "standard output", "unable to dup2()");

		clearerr(stdout);
		close(sd);
		sd = NOTOK;
	} else
		fclose(stdout);

	pidwait(m_pid, OK);
	m_pid = NOTOK;
}
