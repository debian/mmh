/*
** dp.c -- parse dates 822-style
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/fmt_scan.h>
#include <h/tws.h>
#include <locale.h>
#include <sysexits.h>

#define NDATES 100

#define FORMAT "=%<(nodate{text})error: %{text}%|%(putstr(pretty{text}))%>"

static struct swit switches[] = {
#define FORMSW  0
	{ "form formatfile", 0 },
#define VERSIONSW  1
	{ "Version", 0 },
#define HELPSW  2
	{ "help", 0 },
	{ NULL, 0 }
};

char *version=VERSION;

static struct format *fmt;

static int dat[5];

/*
** static prototypes
*/
static int process(char *);


int
main(int argc, char **argv)
{
	int datep = 0, status = 0;
	char *cp, *form = NULL, *fmtstr;
	char buf[BUFSIZ], **argp, **arguments;
	char *dates[NDATES];

	setlocale(LC_ALL, "");
	invo_name = mhbasename(argv[0]);

	/* read user profile/context */
	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf), "%s [switches] dates ...", invo_name);
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case FORMSW:
				if (!(form = *argp++) || *form == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				continue;

			}
		}
		if (datep > NDATES)
			adios(EX_USAGE, NULL, "more than %d dates", NDATES);
		else
			dates[datep++] = cp;
	}
	dates[datep] = NULL;

	if (datep == 0)
		adios(EX_USAGE, NULL, "usage: %s [switches] dates ...", invo_name);

	/* get new format string */
	fmtstr = new_fs(form, FORMAT);

	fmt_compile(fmtstr, &fmt);

	dat[0] = 0;
	dat[1] = 0;
	dat[2] = 0;
	dat[3] = BUFSIZ;
	dat[4] = 0;

	for (datep = 0; dates[datep]; datep++)
		status += process(dates[datep]);

	context_save();  /* save the context file */
	return status;
}


static int
process(char *date)
{
	int status = 0;
	char buffer[BUFSIZ + 1];
	struct comp *cptr;

	FINDCOMP(cptr, "text");
	if (cptr)
		cptr->c_text = date;
	fmt_scan(fmt, buffer, BUFSIZ, dat);
	fputs(buffer, stdout);

	return status;
}
