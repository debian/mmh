/*
** mhparam.c -- print mh_profile values
**
** Originally contributed by
** Jeffrey C Honig <Jeffrey_C_Honig@cornell.edu>
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <sysexits.h>

static struct swit switches[] = {
#define COMPSW  0
	{ "components", 0 },
#define NCOMPSW  1
	{ "nocomponents", 2 },
#define ALLSW  2
	{ "all", 0 },
#define VERSIONSW 3
	{ "Version", 0 },
#define HELPSW  4
	{ "help", 0 },
#define DEBUGSW   5
	{ "debug", -5 },
	{ NULL, 0 }
};

char *version=VERSION;

struct proc {
	char *p_name;
	char **p_field;
};

char *empty = "";

/*
** This list should contain all values of h/mh.h and config/config.c
** TODO: Add the constants Nbby, MAXARGS, NUMATTRS, NAMESZ
*/
static struct proc procs [] = {
	{ "#--Version--", &empty },
	{ "version",          &version },
	{ "lib-version",      &lib_version },

	{ "#--Path-and-File-Names--", &empty },
	{ "mypath",          &mypath },
	{ "mmhdir",          &mmhdir },
	{ "mmhpath",         &mmhpath },
	{ "profile",         &profile },
	{ "defpath",         &defpath },
	{ "context",         &context },
	{ "ctxpath",         &ctxpath },
	{ "mhetcdir",        &mhetcdir },
	{ "mailspool",       &mailspool },
	{ "mailstore",       &mailstore },
	{ "mh-sequences",    &mh_seq },

	{ "#--Default-Programs--", &empty },
	{ "editor",        &defaulteditor },
	{ "pager",         &defaultpager },
	{ "sendmail",      &sendmail },
	{ "listproc",      &listproc },
	{ "whatnowproc",   &whatnowproc },
	{ "mimetypequeryproc", &mimetypequeryproc },

	{ "#--Mail-Folder-Names--", &empty },
	{ "inbox",          &defaultfolder },
	{ "draftfolder",    &draftfolder },
	{ "trashfolder",    &trashfolder },

	{ "#--Profile-and-Context-Component-Names--", &empty },
	{ "curfolder-component",         &curfolder },
	{ "inbox-component",             &inbox },
	{ "mimetypequery-component",     &mimetypequery },
	{ "nmhstorage-component",        &nmhstorage },
	{ "nsequence-component",         &nsequence },
	{ "psequence-component",         &psequence },
	{ "usequence-component",         &usequence },

	{ "#--Mmh-specific-Mail-Header-Names--", &empty },
	{ "attachment-header", &attach_hdr },
	{ "enc-header",        &enc_hdr },
	{ "sign-header",       &sign_hdr },

	{ "#--File-Permissions--", &empty },
	{ "foldprot",      &foldprot },
	{ "msgprot",       &msgprot },

	{ "#--Template-File-Names--", &empty },
	{ "components",        &components },
	{ "digestcomps",       &digestcomps },
	{ "distcomps",         &distcomps },
	{ "forwcomps",         &forwcomps },
	{ "rcvdistcomps",      &rcvdistcomps },
	{ "replcomps",         &replcomps },
	{ "replgroupcomps",    &replgroupcomps },
	{ "mhlformat",         &mhlformat },
	{ "mhlreply",          &mhlreply },
	{ "scanformat",        &scanformat },

	{ "#--Default-Sequence-Names--", &empty },
	{ "seq-all",           &seq_all },
	{ "seq-beyond",        &seq_beyond },
	{ "seq-cur",           &seq_cur },
	{ "seq-first",         &seq_first },
	{ "seq-last",          &seq_last },
	{ "seq-next",          &seq_next },
	{ "previous-sequence", &seq_prev },
	{ "unseen-sequence",   &seq_unseen },
	{ "sequence-negation", &seq_neg },

	{ NULL,            NULL },
};


/*
** static prototypes
*/
static char *p_find(char *);


int
main(int argc, char **argv)
{
	int i, compp = 0, missed = 0;
	int all = 0, debug = 0;
	int components = -1;
	char *cp, buf[BUFSIZ], **argp;
	char **arguments, *comps[MAXARGS];

	invo_name = mhbasename(argv[0]);

	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf), "%s [profile-components] [switches]", invo_name);
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case COMPSW:
				components = 1;
				break;
			case NCOMPSW:
				components = 0;
				break;

			case ALLSW:
				all = 1;
				break;

			case DEBUGSW:
				debug = 1;
				break;
			}
		} else {
			comps[compp++] = cp;
		}
	}

	if (all) {
		struct node *np;

		if (compp)
			advise(NULL, "profile-components ignored with -all");

		if (components >= 0)
			advise(NULL, "-%scomponents ignored with -all",
					components ? "" : "no");

		/* print all entries in context/profile list */
		for (np = m_defs; np; np = np->n_next)
			printf("%s: %s\n", np->n_name, np->n_field);

	} else if (debug) {
		struct proc *ps;

		/*
		** Print the current value of everything in
		** procs array.  This will show their current
		** value (as determined after context is read).
		*/
		for (ps = procs; ps->p_name; ps++)
			printf("%s: %s\n", ps->p_name,
					*ps->p_field ? *ps->p_field : "");

	} else {
		if (components < 0)
			components = compp > 1;

		for (i = 0; i < compp; i++)  {
			char *value;

			value = context_find(comps[i]);
			if (!value)
				value = p_find(comps[i]);
			if (value) {
				if (components)
					printf("%s: ", comps[i]);

				printf("%s\n", value);
			} else
				missed++;
		}
	}

	return missed;
}


static char *
p_find(char *str)
{
	struct proc *ps;

	for (ps = procs; ps->p_name; ps++)
		if (!mh_strcasecmp(ps->p_name, str))
			return (*ps->p_field);

	return NULL;
}
