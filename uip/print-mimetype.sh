#!/bin/sh
#
# print mime type of file, depending on file name extension

if [ $# -eq 0 ] ; then
	echo "Usage: $0 FILE..." >&2
	exit 1
fi

for i do

case "$i" in
*.xls)	type=application/excel;;
*.ppt)	type=application/mspowerpoint;;
*.doc)	type=application/msword;;
*.pdf)	type=application/pdf;;
*.deb)	type=application/x-debian-package;;
*.tar)	type=application/x-tar;;
*.[1-9])	type=application/x-troff-man;;
*.zip)	type=application/zip;;
*.au)	type=audio/basic;;
*.gif)	type=image/gif;;
*.jpg|*.jpeg)	type=image/jpeg;;
*.png)	type=image/png;;
*.ps)	type=application/postscript;;
*.eps)	type=image/eps;;
*.tif|*.tiff)	type=image/tiff;;
*.ico)	type=image/x-icon;;
*.bmp)	type=image/x-ms-bmp;;
*.pnm)	type=image/x-portable-anymap;;
*.pbm)	type=image/x-portable-bitmap;;
*.pgm)	type=image/x-portable-graymap;;
*.ppm)	type=image/x-portable-pixmap;;
*.xbm)	type=image/x-xbitmap;;
*.xpm)	type=image/x-xpixmap;;
*.xwd)	type=image/x-xwindowdump;;
*.eml)	type=message/rfc822;;
*.rtf)	type=text/richtext;;
*.html|*.htm)	type=text/html;;
*.txt)	type=text/plain;;
*.mpg|*.mpeg)	video/mpeg;;
*)
	if file "$i" | grep -q ' text$' ; then
		type=text/plain
	else
		type=application/octet-stream
	fi
esac
echo "$type"

done

