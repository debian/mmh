.\"
.\" %nmhwarning%
.\"
.TH COMP %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
comp \- compose a message
.SH SYNOPSIS
.HP 5
.na
.B comp
.RI [ +folder ]
.RI [ msgs ]
.RB [ \-form
.IR formfile ]
.RB [ \-use " | " \-nouse ]
.RB [ \-editor
.IR editor ]
.RB [ \-whatnowproc
.IR program ]
.RB [ \-Version ]
.RB [ \-help ]
.ad
.SH DESCRIPTION
.B Comp
is used to create a new message to be mailed.  It copies a
message form to the draft being composed and then invokes an editor on
the draft (unless the
.B \-editor
switch with an empty string argument is given,
in which case the initial edit is suppressed).
.PP
The default message form contains the following elements:
.PP
.RS 5
.nf
%components%
.fi
.RE
.PP
If a file named
.RI ` components '
exists in the user's mmh directory,
it will be used instead of this form.  You may specify an alternate
forms file with the switch
.B \-form
.IR formfile .
.PP
You may also start
.B comp
using the contents of an existing message
as the form.  If you supply a
.I +folder
or
.I msg
argument, that
message will be used as the message form.  You may not supply both a
.B \-form
.I formfile
and a
.I +folder
or
.I msg
argument.  The line of
dashes or a blank line must be left between the header and the body of
the message for the message to be identified properly when it is sent
(see
.BR send (1)).
.PP
The switch
.B \-use
directs
.B comp
to continue editing an already
started message.  That is, if a
.B comp
(or
.BR dist ,
.BR repl ,
or
.BR forw )
is terminated without sending the draft, the draft can
be edited again via
.RB ` comp
.BR \-use '.
If
.B \-use
is given,
.I msg
is located within the draft folder.
A
.I +folder
argument is not allowed together with
.BR \-use .
.PP
If the draft already exists,
.B comp
will ask you as to the disposition
of the draft.  A reply of
.B quit
will abort
.BR comp ,
leaving the draft intact;
.B replace
will replace the existing draft with
the appropriate form;
.B list
will display the draft;
.B use
will use the draft for further composition; and
.B refile
.I +folder
will file the draft in the given folder, and give you a new draft with the
appropriate form.  (The
.I +folder
argument to
.B refile
is required.)
.PP
Consult the
.BR mh-draft (7)
man page for more information.
.PP
The
.B \-editor
.I editor
switch indicates the editor to use for the
initial edit.  Upon exiting from the editor,
.B comp
will invoke the
.B whatnow
program.  See
.BR whatnow (1)
for a discussion of
available options.

.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^%etcdir%/components~^The standard message skeleton
^or $HOME/.mmh/components~^Rather than the standard skeleton
^$HOME/.mmh/profile~^The user profile
^+drafts~^The draft folder
.fi

.SH "PROFILE COMPONENTS"
.fc ^ ~
.nf
.ta 2.4i
.ta \w'ExtraBigProfileName  'u
^Path:~^To determine the user's mail storage
^Draft\-Folder:~^To set the default draft folder
^Editor:~^To override the default editor
^Msg\-Protect:~^To set mode when creating a new message (draft)
^whatnowproc:~^Program to ask the `What now?' questions
.fi

.SH "SEE ALSO"
dist(1), forw(1), repl(1), send(1), whatnow(1), mh-profile(5)

.SH DEFAULTS
.nf
.RB ` +folder "' defaults to the current folder"
.RB ` msg "' defaults to the current message"
.RB ` \-nouse '
.fi

.SH CONTEXT
None
