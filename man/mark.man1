.\"
.\" %nmhwarning%
.\"
.TH MARK %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
mark \- manipulate message sequences
.SH SYNOPSIS
.HP 5
.na
.B mark
.RI [ +folder ]
.RI [ msgs ]
.RB [ \-sequence
.I name
\&...]
.RB [ \-add " | " \-delete ]
.RB [ \-list ]
.RB [ \-public " | " \-nopublic ]
.RB [ \-zero " | " \-nozero ]
.RB [ \-Version ]
.RB [ \-help ]
.ad
.SH DESCRIPTION
The
.B mark
command manipulates message sequences by adding or deleting
message numbers from folder\-specific message sequences, or by listing
those sequences and messages.
.PP
A message sequence is a keyword, just like one of the `reserved'
message names, such as `f' or `n' (see mh-sequence(7)).  Unlike the
`reserved' message names, which have a fixed semantics on
a per\-folder basis, the semantics of a message sequence may be
defined, modified, and removed by the user.  Message sequences are
folder\-specific, e.g., the sequence name `seen' in the context
of folder
.RI ` +inbox '
need not have any relation whatsoever to the
sequence of the same name in a folder of a different name.
.PP
Three action switches direct the operation of
.BR mark .
These switches
are mutually exclusive: the last occurrence of any of them overrides
any previous occurrence of the other two.
.PP
The
.B \-add
switch tells
.B mark
to add messages to sequences or to
create a new sequence.  For each sequence named via the
.B \-sequence
.I name
argument (which must occur at least once) the messages named via
.I msgs
(which defaults to `c' if no
.I msgs
are given), are added to the
sequence.  The messages to be added need not be absent from the sequence.
If the
.B \-zero
switch is specified, the sequence will be emptied prior
to adding the messages.  Hence,
.B \-add
.B \-zero
means that each sequence
should be initialized to the indicated messages, while
.B \-add
.B \-nozero
means that each sequence should be appended to by the indicated messages.
.PP
The
.B \-delete
switch tells
.B mark
to delete messages from sequences, and is the dual of
.BR \-add .
For each of the named sequences, the
named messages are removed from the sequence.  These messages need
not be already present in the sequence.  If the
.B \-zero
switch is
specified, then all messages in the folder are added to the sequence
(first creating the sequence, if necessary) before removing the messages.
Hence,
.B \-delete
.B \-zero
means that each sequence should contain
all messages except those indicated, while
.B \-delete
.B \-nozero
means
that only the indicated messages should be removed from each sequence.
As expected, the command
.RB ` mark
.B \-sequence
.I foo
.B \-delete
all'
deletes the sequence `foo' from the current folder.
.PP
When creating or modifying sequences, you can specify the switches
.B \-public
or
.B \-nopublic
to force the new or modified sequences to be
`public' or `private'.  The switch
.B \-public
indicates
that the sequences should be made `public'.  These sequences
will then be readable by all
.B mmh
users with permission to read the relevant folders.  In contrast, the
.B \-nopublic
switch indicates that the
sequences should be made `private', and will only be accessible by
you.  If neither of these switches is specified, then existing sequences
will maintain their current status, and new sequences will default to
`public' if you have write permission for the relevant folder.
Check the
.BR mh\-sequence (7)
man page for more details about the difference
between `public' and `private' sequences.
.PP
The
.B \-list
switch tells
.B mark
to list both the sequences defined
for the folder and the messages associated with those sequences.
.B Mark
will list the name of each sequence given by
.B \-sequence
.I name
and the messages associated with that sequence.  If the
sequence is private, this will also be indicated.  If no sequence is
specified by the
.B \-sequence
switch, then all sequences for this folder
will be listed.  The
.B \-zero
switch does not affect the operation of
.BR \-list .
.PP
The current restrictions on sequences are:
.PP
.IP \(bu 2
The name used to denote a message sequence must consist of an alphabetic
character followed by zero or more alphanumeric characters, and cannot
be one of the (reserved) message names `c', `f',
`l', `a', `n', `p', or `b'.
.PP
.IP \(bu 2
Only a certain number of sequences may be defined for a given folder.
This number is usually limited to 29 (13 on small systems). (The
+internal implementation relies on bitmasks, with some bits set aside
+for internal use.)
.PP
.IP \(bu 2
Message ranges with user\-defined sequence names are restricted to the
form `name:n', `name:+n', or `name:-n', and refer
to the first or last `n' messages of the sequence `name', respectively.
Constructs of the form `name1\-name2' are forbidden for user
defined sequences.

.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^$HOME/.mmh/profile~^The user profile
.fi

.SH "PROFILE COMPONENTS"
.fc ^ ~
.nf
.ta 2.4i
.ta \w'ExtraBigProfileName  'u
^Path:~^To determine the user's mail storage
^Current\-Folder:~^To find the default current folder
.fi

.SH "SEE ALSO"
flist(1), pick(1), mh-sequence(7)

.SH DEFAULTS
.nf
.RB ` +folder "' defaults to the current folder"
.RB ` \-add "' if " \-sequence " is specified, " \-list " otherwise"
.RB ` msgs "' defaults to `c' (or `a' if " \-list " is specified)"
.RB ` \-nozero '

.SH CONTEXT
If a folder is given, it will become the current folder.

.SH "HELPFUL HINTS"
Use
.B flist
to find folders with a given sequence, and
.RB ` pick
.I sequence
.BR \-list '
to enumerate those messages in the sequence (such as for
use by a shell script).
